---

## Presentation

this application tile differents images from the satellite Sentinel2 by only use his ZipFile. the differents regions tiled for each resolution(60m,20m,10m) have the same area.


---

## Usage

./main.py ZipFile TileWitdh TileHeight Mode pourcentNoDate(default :10) CloudProbability(default: 50) DestinationRepository

ZipFile : Sentinel2 ZipFile

TileWidth : witdh of a tile in 10m resolution

TileHeight : Height of a tile in 10m resolution

Mode :

	-Padding : If a tile exceeds the image , we will fill it with no Data region to respect the dimensiosn of a tile.(tileWidth*tileHeight).

	-Crop : we don't exceeds the image and only take what's left on the image even if it does'nt repect the dimensions of a tile(tileWidth*tileHeight).

	-Stride : If a tile exceeds the image , we will move the tile to only contain Data from the image and respect the dimensions of a tile(tileWidth*tileHeight).

pourcentNoData : pourcent of acceptable no data for a tile.

CloudProbability : if a region have more than CLoudProbability% chance to be a cloud , this regions will be considerate like a no data region.

DestinationRepository : repository where the differents images will be save

Example :

	./main.py ./S2A_MSIL2A_20190416T022551_N0211_R046_T50NLM_20190416T062614.zip 1024 1024 crop 25 75 ./img
---
