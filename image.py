#!/usr/bin/python3
import rasterio



class Image:

    def __init__(self,name,base):
        self.name = name
        self.base = base
        self.data = rasterio.open(self.name)
    
        self.width = self.data.shape[1]
        self.height = self.data.shape[0]
        self.crs = self.data.crs
        self.transform = self.data.transform
        self.nbBands = self.data.count

        self.left = self.data.bounds.left
        self.right = self.data.bounds.right
        self.bottom = self.data.bounds.bottom
        self.top = self.data.bounds.top

    def display(self):
        print("name = ",self.name)
        print("base = ",self.base)
        print("name = ",self.data)
        print("width = ",self.width)
        print("height = ",self.height)
        print("crs = ",self.crs)
        print("bound Left = ",self.left)
        print("bound Bottom = ",self.bottom)
        print("Bound Right = ",self.right)
        print("Bound Top = ",self.top)


    #this function return every bounding box for each tile we can make with the image.
    def getTilesBounds(self,mode,tileWidth,tileHeight):

        nbTileX=0
        nbTileY=0
        tab=[]

        #number of tile i can do with these dimension
        while(nbTileY*tileHeight<self.height):
            nbTileY+=1


        while(nbTileX*tileWidth<self.width):
            nbTileX+=1


        for i in range(nbTileY):
            for j in range (nbTileX):


                left= (self.transform * (tileWidth*j, 0))[0]
                bottom= (self.transform * (0, tileHeight*(i+1)))[1]
                right= (self.transform * (tileWidth*(j+1), 0))[0]
                top= (self.transform * (0, tileHeight*i))[1]


                if (mode=="stride"):

                    if(right > self.right):
                        right = self.right
                        left = (self.transform * (self.width-tileWidth,0))[0]


                    if(bottom < self.bottom):
                        bottom=self.bottom
                        top = (self.transform * (0,self.height-tileHeight))[1]

                #in a first time , padding mode will have the same bounding box than crop mode to don't have issue 
                #when we gonna read the image in the right region , but after we gonna fill the tile to respect the size of a tile.
                elif (mode=="crop" or "padding"):

                    if(right > self.right):
                        right = self.right

                    if(bottom < self.bottom):
                        bottom = self.bottom

                tileBox = {"left":left, "bottom":bottom, "right":right, "top":top}

                tab.append(tileBox)

        return tab


    