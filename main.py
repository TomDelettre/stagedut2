#!/usr/bin/python3
import sys
import tile

if __name__ == '__main__':

	if (len(sys.argv)<5):
		print("Usage : Sentinel2ZipFile  tileWidth  tileHeight  mode  pourcentNoData(default :25)  cloubProbability(default :50)  dst_repository\n")
		sys.exit(1)

	zipFile=sys.argv[1]
	tileWidth=int(sys.argv[2])
	tileHeight=int(sys.argv[3])
	mode=sys.argv[4]

	if (6<len(sys.argv)):
		pourcentNodata=int(sys.argv[5])
	else :
		pourcentNodata=10

	if (7<len(sys.argv)):
		cloudProbability=int(sys.argv[6])
	else :
		cloudProbability=50

	dst_repository=sys.argv[len(sys.argv)-1]

	tile.tiling(zipFile,tileWidth,tileHeight,mode,pourcentNodata,cloudProbability,dst_repository)

