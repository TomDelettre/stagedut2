#!/usr/bin/python3
import zipfile
import untangle
import sys, os
import xmltodict
import json




def getCloudProbMask(ZipFile,base):

     if(base!=60 and base !=20):
          print('Usage: {} sentinel2_zip 20|60'.format(sys.argv[0]))

          sys.exit(1)

     s2_manifest = None
     qi_msk = { '20': 'Preview_0_Tile1_Unit', '60': 'Preview_1_Tile1_Unit' }

     with zipfile.ZipFile(ZipFile) as zp:

          for f in zp.filelist:

               if os.path.basename(f.filename) == 'manifest.safe':

                    with zp.open(f.filename, 'r') as zf:

                         s2_manifest = json.loads(json.dumps(xmltodict.parse(zf.read())))


     for i in (s2_manifest['xfdu:XFDU']['informationPackageMap']['xfdu:contentUnit']['xfdu:contentUnit']):

          if i['@ID'] == 'Tiles':

               for j in i['xfdu:contentUnit']['xfdu:contentUnit']:

                    if j['@ID'] == 'QI_DATA_Tile1':

                         for k in j['xfdu:contentUnit']:

                              if k['@ID'] == qi_msk[str(base)]:

                                   for o in s2_manifest['xfdu:XFDU']['dataObjectSection']['dataObject']:

                                        if o['@ID'] == k['dataObjectPointer']['@dataObjectID']:

                                             path,fileName=parse(ZipFile)

                                             #Absolute path to the repository who contain the zipFile
                                             path=os.path.abspath(path)

                                             safeFile=fileName[:-3]+"SAFE"
                                             
                                             cloudPath="/vsizip/"+path+"/"+fileName+"/"+safeFile+"/"+(o['byteStream']['fileLocation']['@href'][2:])

                                             return cloudPath



def parse(zipFile):
     i=len(zipFile)
     while(i!=0 and zipFile[i-1]!="/"):
          i-=1

     return zipFile[:i],zipFile[i:]
