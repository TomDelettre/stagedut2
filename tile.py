#!/usr/bin/python3
import os
import sys
import numpy as np

import rasterio
from rasterio.enums import Resampling
from rasterio.vrt import WarpedVRT
from rasterio.crs import CRS

from image import Image
import path

#this is the main function , he gonna call all the other function.
def tiling(zipFile,tileWidth,tileHeight,mode,pourcentNoData,cloudProbability,dst_repository):

	data=rasterio.open(zipFile)

	#subdatasets
	image60m=Image(data.subdatasets[2],60)
	image20m=Image(data.subdatasets[1],20)
	image10m=Image(data.subdatasets[0],10)

	#this function check any basic issue
	check(image10m,tileWidth,tileHeight,mode,pourcentNoData,cloudProbability,dst_repository)

	#Size of tile in 60 meter resolution
	tileWidth60m=int(changeBase(tileWidth,tileHeight,image10m.data,image60m.data)[0])
	tileHeight60m=int(changeBase(tileWidth,tileHeight,image10m.data,image60m.data)[1])
	
	#Size of tile in 20 meter resolution
	tileWidth20m=int(changeBase(tileWidth,tileHeight,image10m.data,image20m.data)[0])
	tileHeight20m=int(changeBase(tileWidth,tileHeight,image10m.data,image20m.data)[1])


	#this is all the bounding box for each tile of a image
	tilesBounds60m=image60m.getTilesBounds(mode,tileWidth60m,tileHeight60m)

	#this is the cloudmask_probability in the 60 meter resolution image.
	cloud=rasterio.open(path.getCloudProbMask(zipFile,60))

	#validTiles is an array with all the bounding box for each tile who are "Valid" (respect the pourcent of no data).
	#for question of efficiency , we only check the 60m tiles 
	validTiles=getValidTiles(image60m,tilesBounds60m,cloud,pourcentNoData,cloudProbability,mode,nbValues=(tileWidth60m*tileHeight60m))

	#create tile en the right repository in the tiff format.
	createTile(image60m,validTiles,dst_repository,mode,tileWidth60m,tileHeight60m)
	print("tiling the 60 meter resolution image done\n")
	createTile(image20m,validTiles,dst_repository,mode,tileWidth20m,tileHeight20m)
	print("tiling the 20 meter resolution image done\n")
	createTile(image10m,validTiles,dst_repository,mode,tileWidth,tileHeight)
	print("tiling the 10 meter resolution image done\n")

#this function check any basic issue
def check(image,tileWidth,tileHeight,mode,pourcentNodata,cloudProbability,dst_repository):

	if( tileHeight>image.height or tileWidth>image.width ) :
		print("Error : the size of the tile you want is higher than the size of the image.")
		sys.exit(1)

	elif( os.path.isdir(dst_repository)==False ) :
		print("Error : the destination repository didn't exit.")
		sys.exit(1)

	elif( mode!="crop" and mode!="stride" and mode!="padding" ) :
		print("Error : the tiling mode is wrong , please choose one of the following modes : \ncrop , stride or padding")
		sys.exit(1)

	elif not( 0<=pourcentNodata<=100 ) :
		print("Error : pourcentage of no Data must be a number between 0 and 100.")
		sys.exit(1)

	elif not( 0<=cloudProbability<=100 ) :
		print("Error : cloud probability must be a number between 0 and 100.")
		sys.exit(1)



#Dimension for one tile in "scene" meter r�solution
def changeBase(width,height,base_scene,scene):

	x_geo_orig, y_geo_orig = base_scene.xy(width, height, offset='ul')

	computed_shape = scene.index(x_geo_orig, y_geo_orig)

	return computed_shape

#return an array with all the bouding box for each tile who are valid (respect the no data pourcent)
def getValidTiles(image,tilesBounds,cloud,pourcentNodata,cloudProbability,mode,nbValues):

		with rasterio.open(image.name) as src :
			with WarpedVRT(src, crs=image.crs,
						   resampling=Resampling.bilinear) as vrtImage:

				validTiles=[]
				#create an 60m tile for each bounding box with have too check if it's a valid tile.
				cpt=0
				for tileBox in tilesBounds :
					cpt+=1
					dst_window = vrtImage.window(tileBox["left"],tileBox["bottom"],tileBox["right"],tileBox["top"])
					tileData = vrtImage.read(window=dst_window, out_shape=(vrtImage.count, int(dst_window.height), int(dst_window.width)))

					#first band of the tileData , just for check no data region.
					tileData_band1=tileData[0]
					#array of the cloud_mask region who fit with the tileData region
					cloudTile=getCloudTile(dst_window,cloud)
					
					#if the tileData respect the pourcent of no data , then it's a valid tile and we copy his bounding box for after create the tile in 60m/20m/10m resolution
					#In the padding mode , tileData don't have yet region who exceeds the initial image , so to have the right number of totalValue who gonna have the tile ,
					#we mutiply tileWidth60m*tileHeight60m (initial size of a tile in 60m resolution)
					if(isValidTile(tileData_band1,cloudTile,pourcentNodata,cloudProbability,mode,nbValues)):
						print("tile number" , cpt , "Valid")
						validTiles.append(tileBox)

					else :
						print("tile number" , cpt , "not valid")
				print("\n")
		return validTiles


#return the right region of the cloud_mask who fit with the tile.
def getCloudTile(dst_window,cloud):

	left = int(dst_window.col_off)
	upper = int(dst_window.row_off)
	bottom = int(dst_window.row_off+dst_window.height)
	right = int(dst_window.col_off+dst_window.width)
	
	cloudProbability=cloud.read(1)
	tileCloudProbability = []

	for i in range (upper,bottom):

		tmp= cloudProbability[i][left:right]
		tileCloudProbability.append(tmp)

	return tileCloudProbability

#this function browse the cloud array and the tileData array to find 0 region values and return true if the tile is acceptable
#nbValues is the number of value who have an initial tile.
def isValidTile(tileData,cloud,pourcentNodata,cloudProbability,mode,nbValues):

	if(len(tileData)!=len(cloud) or len(tileData[0])!=len(cloud[0])):
		print("Error , the tileCLoud and the tileData don't have the same shape")
		sys.exit(1)


	nbTileValues=len(tileData)*len(tileData[0])
	nbData=0
	nbNodata=0

	#in padding mode , the tile may exceed the initial image , if it's th case , we already have nodata value who is equal to nbValues-nbTileValues
	#nbValues = number of value who have an initial tile .
	#nbTileValues = number of value who have the current tile.
	if(mode!="padding"):
		nbValues=nbTileValues
	else :
		nbNodata=nbValues-nbTileValues

	if(pourcentNodata==0):
		accetableLostValues=0
	else :
		v1=100/pourcentNodata
		accetableLostValues=nbValues/v1

	for i in range (len(tileData)):
		for j in range (len(tileData[0])):

			if((nbData)>(nbValues-accetableLostValues)):
				return True

			elif(nbNodata>accetableLostValues):
				return False

			elif(tileData[i][j]==0 or tileData[i][j]==None or cloud[i][j]>cloudProbability):
				nbNodata+=1
				
			else :
				nbData+=1

	return True

def createTile(image,validTiles,dst_repository,mode,tileWidth,tileHeight):
		
	with rasterio.open(image.name) as src :
		with WarpedVRT(src, crs=image.crs,
			esampling=Resampling.bilinear) as vrtImage:


				cpt=0
				for tileBox in validTiles:
					cpt+=1

					dst_window = vrtImage.window(tileBox["left"],tileBox["bottom"],tileBox["right"],tileBox["top"])
					tileData = vrtImage.read(window=dst_window, out_shape=(vrtImage.count, int(dst_window.height), int(dst_window.width)))
					
					#padding mode must repesct the inital size of a tile , so we fill it with 0 value if his size is lower than the inital size of a tile
					if (mode=="padding" and (tileData.shape[1]<tileHeight or tileData.shape[2]<tileWidth )) :
							tileData=fillTheTile(tileData,dst_window,tileWidth,tileHeight)

					profile = vrtImage.profile
					profile['width'] = tileData.shape[2]
					profile['height'] = tileData.shape[1]
					profile['driver'] = 'GTIFF'
					#deleting the blocksize allow us to create image lower than 512*512 pixel
					del profile['blockxsize']
					del profile['blockysize']
					profile['tiled'] = False
					dst_transform = vrtImage.window_transform(dst_window)
					profile['transform'] = dst_transform

					file_name="tile_"+str(image.base)+"m_"+str(cpt)+'.tiff'
					new_file =dst_repository+"/"+file_name

					writeImage(new_file,profile,tileData)

#This function fill the tileData with "0" value to respect the padding mode.
def fillTheTile(tileData,dst_window,tileWidth,tileHeight) :
	data=[]

	for i in range(tileData.shape[0]):
		band=[]

		for j in range(tileHeight):

			if(j>=dst_window.height):
				tmp=[]

				for k in range(tileWidth):
					tmp.append(0)

			else :
				tmp=tileData[i][j].tolist()

				for k in range(int(dst_window.width),tileWidth):
					tmp.append(0)

			band.append(tmp)
		data.append(band)		

	newTileData=np.asarray(data , dtype=tileData.dtype)

	return newTileData


def writeImage(file,profile,data):

	with rasterio.open(file, 'w', **profile) as dst:
		dst.write(data)

	print(file , "done")
